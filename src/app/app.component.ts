import { Component } from '@angular/core';
import {car} from './shared/models/Car';
import {CarsService} from './shared/services/cars.service';
import { FormGroup, FormControl } from '@angular/forms';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(
    private getCarsService: CarsService,
    private fb: FormBuilder
    ){};
  title = 'car-dealership';
  
  cars:car[];
  gotthecars: boolean = false;

  optionForm = new FormGroup({
    _id: new FormControl(''),
    make: new FormControl(''),
    price: new FormControl(''),
    year: new FormControl(''),
    color: new FormControl(''),
    hasSunroof: new FormControl(''),
    isFourWheelDrive: new FormControl(''),
    hasLowMiles: new FormControl(''),
    hasPowerWindows: new FormControl(''),
    hasNavigation: new FormControl(''),
    hasHeatedSeats: new FormControl(''),
  });

  onSubmit() {
    
    console.log(this.optionForm.value);
    this.getCars(this.optionForm.value);
    
  }


  ngOnInit(){
    // this.getCars(this.optionForm.value);
    
  };
  getCars( filter: car){
     this.getCarsService.getCars(filter).subscribe(cars =>{this.cars = cars});
     this.gotthecars = true;
  }
}
