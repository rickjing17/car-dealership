import { Injectable } from '@angular/core';
import {car} from './../models/Car';
import carsData from './../mock-data/cars-data';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CarsService {
  constructor() {}
  
  // TODO: methods to return data from json file or create an in memory web api
  testFilter:car ={
    _id: '',
        make: 'Toyota',
        year: 2012,
        color: 'Silver',
        price: 0,
        hasSunroof: true,
        isFourWheelDrive: true,
        hasLowMiles: false,
        hasPowerWindows: true,
        hasNavigation: false,
        hasHeatedSeats: true
  };
  getCars( filter: car){
    let allcars = carsData;
    console.log(allcars);
    let res = allcars.filter((x)=>{
      
      return this.isObjEqual(filter, x);
      
    })
    // console.log(res);
    return of (res);
  }

  isObjEqual(o1: car,o2: car){
    let props1 = Object.getOwnPropertyNames(o1);
    
    for (let i = 0,max = props1.length; i < max; i++) {
      
      let propName = props1[i];
      if(propName ==="price" || propName ==="_id") continue;
      if((<any>o1)[propName]==='' || (<any>o1)[propName]=== null || (<any>o1)[propName]=== false ) continue;
      if ((<any>o1)[propName] !== (<any>o2)[propName]) {
        return false;
      }
    }
    return true;

  }
}
