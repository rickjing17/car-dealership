// TODO: Create car model for project
export class car {
    _id: string;
    make: string;
    year: number;
    color: string;
    price: number;
    hasSunroof: boolean;
    isFourWheelDrive: boolean;
    hasLowMiles: boolean;
    hasPowerWindows: boolean;
    hasNavigation: boolean;
    hasHeatedSeats: boolean;
}